// snowflake-server-load simulates many concurrent connections to a
// snowflake-server.
//
// # Usage
//
// The only require argument is a URL to the WebSocket listener port of a
// snowflake-server instance.
//
//	snowflake-server-load ws://localhost:3000/
//
// You may start a snowflake-server instance manually as follows:
//
//	TOR_PT_MANAGED_TRANSPORT_VER=1 \
//	TOR_PT_SERVER_BINDADDR=snowflake-127.0.0.1:3000 \
//	TOR_PT_SERVER_TRANSPORTS=snowflake TOR_PT_ORPORT=127.0.0.1:4000 \
//	./server -disable-tls -unsafe-logging
//
// The TOR_PT_ORPORT may point to anything, for example a netcat echo server:
//
//	ncat -l -k -v --sh-exec cat 127.0.0.1 4000
//
// # Options
//
//	-num
//		The number of connections to make.
//	-duration
//		How long each connection should stay connected.
//	-numbytes
//		The number of bytes each connection should send.
package main

import (
	"bufio"
	"crypto/rand"
	"errors"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"git.torproject.org/pluggable-transports/snowflake.git/v2/common/encapsulation"
	"git.torproject.org/pluggable-transports/snowflake.git/v2/common/turbotunnel"
	"git.torproject.org/pluggable-transports/snowflake.git/v2/common/websocketconn"
	"github.com/gorilla/websocket"
	"github.com/xtaci/kcp-go/v5"
	"github.com/xtaci/smux"
)

// encapsulationPacketConn implements the net.PacketConn interface over an
// io.ReadWriteCloser stream, using the encapsulation package to represent
// packets in a stream.
type encapsulationPacketConn struct {
	rwc        io.ReadWriteCloser
	localAddr  net.Addr
	remoteAddr net.Addr
	bw         *bufio.Writer
}

// newEncapsulationPacketConn creates a new encapsulationPacketConn over the
// given io.ReadWriteCloser.
func newEncapsulationPacketConn(localAddr, remoteAddr net.Addr, rwc io.ReadWriteCloser) *encapsulationPacketConn {
	return &encapsulationPacketConn{
		rwc:        rwc,
		localAddr:  localAddr,
		remoteAddr: remoteAddr,
	}
}

// ReadFrom reads an encapsulated packet from the stream.
func (c *encapsulationPacketConn) ReadFrom(p []byte) (int, net.Addr, error) {
	data, err := encapsulation.ReadData(c.rwc)
	if err != nil {
		return 0, c.remoteAddr, err
	}
	return copy(p, data), c.remoteAddr, nil
}

// WriteTo writes an encapsulated packet to the stream.
func (c *encapsulationPacketConn) WriteTo(p []byte, addr net.Addr) (int, error) {
	// addr is ignored.
	_, err := encapsulation.WriteData(c.rwc, p)
	if err != nil {
		return 0, err
	}
	return len(p), nil
}

func (c *encapsulationPacketConn) Close() error                       { return c.rwc.Close() }
func (c *encapsulationPacketConn) LocalAddr() net.Addr                { return c.localAddr }
func (c *encapsulationPacketConn) SetDeadline(t time.Time) error      { panic("not implemented") }
func (c *encapsulationPacketConn) SetReadDeadline(t time.Time) error  { panic("not implemented") }
func (c *encapsulationPacketConn) SetWriteDeadline(t time.Time) error { panic("not implemented") }

type emptyAddr struct{}

func (_ emptyAddr) Network() string { return "empty" }
func (_ emptyAddr) String() string  { return "empty" }

var stats struct {
	numBytesRead    int64
	numBytesWritten int64
}

func run(serverURL string, num int, duration time.Duration, numBytes int64) error {
	var wg sync.WaitGroup
	wg.Add(num)
	for i := 0; i < num; i++ {
		go func() {
			err := runOne(serverURL, duration, numBytes)
			if err != nil {
				fmt.Fprintf(os.Stderr, "connection %v: %v\n", i, err)
			}
			wg.Done()
		}()
	}
	wg.Wait()
	return nil
}

func runOne(serverURL string, duration time.Duration, numBytes int64) error {
	ws, _, err := websocket.DefaultDialer.Dial(serverURL, nil)
	if err != nil {
		return err
	}
	wsConn := websocketconn.New(ws)
	defer wsConn.Close()

	_, err = wsConn.Write(turbotunnel.Token[:])
	if err != nil {
		return err
	}
	clientID := turbotunnel.NewClientID()
	_, err = wsConn.Write(clientID[:])
	if err != nil {
		return err
	}

	pconn := newEncapsulationPacketConn(emptyAddr{}, emptyAddr{}, wsConn)
	conn, err := kcp.NewConn2(emptyAddr{}, nil, 0, 0, pconn)
	if err != nil {
		return err
	}
	defer conn.Close()
	smuxConfig := smux.DefaultConfig()
	smuxConfig.Version = 2
	smuxConfig.KeepAliveTimeout = 10 * time.Minute
	sess, err := smux.Client(conn, smuxConfig)
	if err != nil {
		return err
	}
	stream, err := sess.OpenStream()
	if err != nil {
		return err
	}

	go func() {
		n, err := io.Copy(io.Discard, stream)
		atomic.AddInt64(&stats.numBytesRead, n)
		if errors.Is(err, io.ErrClosedPipe) {
			err = nil
		}
		if err != nil {
			fmt.Fprintf(os.Stderr, "read error: %v\n", err)
		}
	}()

	var buf [1024]byte
	_, err = rand.Read(buf[:])
	if err != nil {
		return err
	}

	var n int64
	start := time.Now()
	chunkSize := len(buf)
	for {
		dt := time.Since(start)
		if n >= numBytes && dt >= duration {
			break
		}

		t := time.Duration(float64(n)/float64(numBytes)*float64(duration.Nanoseconds())) * time.Nanosecond

		if dt < t {
			time.Sleep(t - dt)
		} else {
			if numBytes-n < int64(chunkSize) {
				chunkSize = int(numBytes - n)
			}
			x, err := stream.Write(buf[:chunkSize])
			n += int64(x)
			atomic.AddInt64(&stats.numBytesWritten, int64(x))
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func main() {
	var num int
	var duration time.Duration
	var numBytes int64

	flag.IntVar(&num, "num", 50, "number of connections to make")
	flag.DurationVar(&duration, "duration", 10*time.Second, "how long to keep each connection")
	flag.Int64Var(&numBytes, "numbytes", 100000, "number of bytes to send on each connection")

	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Fprintln(os.Stderr, "error: need a server URL (ws:// or wss://)")
		os.Exit(1)
	}
	serverURL := flag.Arg(0)

	fmt.Printf("%v connections, %v duration, %v bytes per connection\n",
		num, duration, numBytes)

	err := run(serverURL, num, duration, numBytes)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	fmt.Printf("bytes read:    %v\n", atomic.LoadInt64(&stats.numBytesRead))
	fmt.Printf("bytes written: %v\n", atomic.LoadInt64(&stats.numBytesWritten))
}
